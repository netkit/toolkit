# web常用工具箱

[English](README.en.md)

#### 介绍

web常用工具箱
- 工具类： 
    - bean拷贝工具类
    - json序列化工具类
- 常用接口
    - 缓存键定义接口: CacheKeyAble
    - 可拷贝的bean接口: Convertible
    - 可转换为map的接口: Mappable
    - 响应码接口: ResponseCode
- 常用异常
    - 通用响应异常类： ResponseException
    - 业务异常类: ServiceException
    - 验证异常类: ValidateException
- 常用响应相关类
    - 接口响应类: ApiResponse
    - 分页响应类： PageList


#### 使用说明

##### 1.添加maven依赖

```xml
<dependency>
    <groupId>top.netkit</groupId>
    <artifactId>toolkit</artifactId>
    <version>1.0.2</version>
</dependency>

```

##### 2. 基本使用

(1)bean拷贝

```java
import top.netkit.toolkit.base.Convertible;
public class Item implements Convertible {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
}

public class ItemVO {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
}

public class Demo {

    public static void main(String[] args){
        Item item = new Item();
        //其他操作查询出item信息      
        ItemVO itemVO = item.convert(ItemVO.class);
    }

}
```
(2)json工具类

```java
public class Demo {
    public static void main(String[] args){
      String data = "{\"id\":1,\"name\":\"netkit\"}";
      Map<String, Object> dataMap = JsonUtil.parseObject(data, Map.class);
      String jsonString = JsonUtil.toJsonString(dataMap);
    }
}
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 feature/feature分支
3.  提交代码
4.  新建 Pull Request

