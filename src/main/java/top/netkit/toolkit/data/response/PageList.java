package top.netkit.toolkit.data.response;



import java.util.ArrayList;
import java.util.List;

/**
 * Pagination
 * @author shixinke
 * @version 1.0
 */
public class PageList<T> {
    /**
     * total records
     */
    private long total;
    /**
     * page number
     */
    private int page;
    /**
     * page size
     */
    private int pageSize;
    /**
     * total pages
     */
    private int pages;
    /**
     * data list
     */
    private List<T> list;


    public PageList(long total, int page, int pageSize, List<T> list) {
        this.total = total;
        this.page = page;
        this.pageSize = pageSize;
        this.pages = calcPages(total, pageSize);
        if (list == null) {
            list = new ArrayList<>(0);
        }
        this.list = list;
    }

    public PageList() {
        this(0L, 1, 10, null);
    }

    public PageList(long total, int pageSize) {
        this(total, 1, pageSize, null);
    }

    public PageList(long total, int page, int pageSize) {
        this(total, page, pageSize, null);
    }

    public static int calcPages(long total, int pageSize) {
        if (total <= pageSize) {
            return 1;
        }
        int remain = (int) (total % pageSize);
        int pages = (int) (total / pageSize);
        if (remain != 0) {
            pages += 1;
        }
        return pages;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
