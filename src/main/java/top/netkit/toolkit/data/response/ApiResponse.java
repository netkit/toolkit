package top.netkit.toolkit.data.response;


import net.sf.cglib.core.ReflectUtils;
import top.netkit.toolkit.base.ResponseCode;
import top.netkit.toolkit.exception.ResponseException;

/**
 * @author shixinke
 * @version 1.0
 * created 19-2-22 下午2:07
 */
public class ApiResponse<T> {
    private Integer code;
    private String message;
    private boolean success;
    private T data;

    private static final int SUCCESS_CODE = 200;
    private static final String SUCCESS_MESSAGE = "success";
    private static final int ERROR_CODE = 500;
    private static final String ERROR_MESSAGE = "fail";

    public ApiResponse() {
        this.code = SUCCESS_CODE;
        this.message = SUCCESS_MESSAGE;
        this.success = true;
        this.data = (T) new Object();
    }

    public ApiResponse(int code) {
        this();
        this.code = code;
    }

    public ApiResponse(String message) {
        this();
        this.message = message;
    }

    public ApiResponse(T data) {
        this();
        this.data = data;
    }

    public ApiResponse(int code, String message) {
        this();
        this.code = code;
        this.message = message;
    }

    public ApiResponse(int code, T data) {
        this();
        this.code = code;
        this.data = data;
    }

    public ApiResponse(int code, String message, T data) {
        this();
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResponse(int code, String message, boolean success, T data) {
        this.code = code;
        this.message = message;
        this.success = success;
        this.data = data;
    }

    public ApiResponse<T> setError(int code, String message) {
        this.setCode(code);
        this.setSuccess(false);
        this.setMessage(message);
        return this;
    }


    public ApiResponse<T> setError(ResponseException ex) {
        this.setError(ex.getCode(), ex.getMessage());
        return this;
    }

    public ApiResponse<T> setError(ResponseCode error) {
        return setError(error.getCode(), error.getMessage());
    }

    public ApiResponse <T> setEmpty(String message, Class<T> clazz) {
        this.setCode(SUCCESS_CODE);
        this.setMessage(message);
        this.setSuccess(false);
        try {
            this.setData((T) ReflectUtils.newInstance(clazz));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }


    public static ApiResponse<Boolean> success() {
        return new ApiResponse<>(SUCCESS_CODE, SUCCESS_MESSAGE, true);
    }

    public static ApiResponse<Boolean> success(int code) {
        return new ApiResponse<>(code, SUCCESS_MESSAGE, true);
    }

    public static ApiResponse<Boolean> success(String message) {
        return new ApiResponse<>(SUCCESS_CODE, message, true);
    }

    public static <T> ApiResponse<T> success(T data) {
        return new ApiResponse<>(SUCCESS_CODE, SUCCESS_MESSAGE, true, data);
    }

    public static <T> ApiResponse<T> success(int code, String message) {
        return new ApiResponse<>(code, message, true, null);
    }

    public static <T> ApiResponse<T> success(int code, T data) {
        return new ApiResponse<>(code, SUCCESS_MESSAGE, true, data);
    }

    public static <T> ApiResponse<T> success(String message, T data) {
        return new ApiResponse<>(SUCCESS_CODE, message, true, data);
    }

    public static <T> ApiResponse<T> success(int code, String message, T data) {
       return new ApiResponse<>(code, message, true, data);
    }

    public static <T> ApiResponse<T> error() {
        return new ApiResponse<>(ERROR_CODE, ERROR_MESSAGE, false, null);
    }

    public static <T> ApiResponse<T> error(int code) {
        return new ApiResponse<>(code, ERROR_MESSAGE, false, null);
    }

    public static <T> ApiResponse<T> error(String message) {
        return new ApiResponse<>(ERROR_CODE, message, false, null);
    }

    public static <T> ApiResponse<T> error(T data) {
        return new ApiResponse<>(ERROR_CODE, ERROR_MESSAGE, false, data);
    }

    public static <T> ApiResponse<T> error(ResponseException ex) {
        return error(ex.getCode(), ex.getMessage());
    }

    public static <T> ApiResponse<T> error(int code, String message) {
        return new ApiResponse<>(code, message, false, null);
    }

    public static <T> ApiResponse<T> error(int code, T data) {
        return new ApiResponse<>(code, ERROR_MESSAGE, false, data);
    }

    public static <T> ApiResponse<T> error(String message, T data) {
        return new ApiResponse<>(ERROR_CODE, message, false, data);
    }

    public static <T> ApiResponse<T> error(int code, String message, T data) {
        return new ApiResponse<>(code, message, false, data);
    }

    public static <T> ApiResponse<T> error(ResponseCode responseCode) {
        if (responseCode.isSuccess()) {
            return success(responseCode.getCode(), responseCode.getMessage());
        }
        return error(responseCode.getCode(), responseCode.getMessage());
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", success=" + success +
                ", data=" + data +
                '}';
    }
}

