package top.netkit.toolkit.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * json util
 * @author shixinke
 */
public class JsonUtil {
    /**
     * objectMapper
     */
    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        OBJECT_MAPPER.setVisibility(OBJECT_MAPPER.getSerializationConfig()
                .getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        OBJECT_MAPPER.enable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN);
        OBJECT_MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        OBJECT_MAPPER.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        OBJECT_MAPPER.findAndRegisterModules();
    }

    public static ObjectMapper getObjectMapper() {
        return OBJECT_MAPPER;
    }

    /**
     * string to Object
     * @param <T> class object
     * @param jsonString source json string
     * @param tr target object
     * @return T
     */
    public synchronized static <T> T parseObject(String jsonString, TypeReference<T> tr) {
        if (jsonString != null && !("".equals(jsonString))) {
            try {
                return (T) (tr.getType().equals(String.class) ? jsonString : OBJECT_MAPPER.readValue(jsonString, tr));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * object to string
     * @param object target object
     * @return json string
     */
    public  static String toJsonString(Object object) {
        String jsonString = "";
        if (object instanceof String) {
            return (String) object;
        }
        try {
            jsonString = OBJECT_MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    /**
     * json string to object
     * @param <T> class object
     * @param jsonString source string
     * @param clazz target class object
     * @return java object
     */
    public static <T> T parseObject(String jsonString, Class<T> clazz) {
        try {
            return OBJECT_MAPPER.readValue(jsonString, clazz);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * json string to  list
     * @param <T> class object
     * @param jsonString source string
     * @param clazz target class
     * @return List
     */
    public static <T> List<T> parseArray(String jsonString, Class<T> clazz) {
        return parseObject(jsonString, new TypeReference<List<T>>() {
        });
    }

    /**
     * object to java object
     * @param <T> class object
     * @param obj source object
     * @param clazz target class
     * @return T
     */
    public static <T> T toJavaObject(Object obj, Class<T> clazz) {
        return parseObject(toJsonString(obj), clazz);
    }

    /**
     * object to map
     * @param data source data
     * @return Map
     */
    public static <K, V> Map<K, V> toMap(Object data) {
        return parseObject(toJsonString(data), new TypeReference<Map<K, V>>(){});
    }

}
