package top.netkit.toolkit.util;

import net.sf.cglib.beans.BeanCopier;
import net.sf.cglib.core.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * bean copy util
 *
 * @author shixinke
 */
public class BeanCopierUtil {


    /**
     * bean cache map
     */
    private static final Map<String, BeanCopier> BEAN_COPIER_MAP = new HashMap<>(100);

    /**
     * object copy
     *
     * @param sourceObj source object
     * @param destObj   destination object
     */
    public static void copy(Object sourceObj, Object destObj) {
        copy(sourceObj, destObj, null);
    }

    /**
     * object copy with converter
     *
     * @param sourceObj source object
     * @param destObj   destination object
     * @param converter converter
     */
    public static void copy(Object sourceObj, Object destObj, Converter converter) {
        String beanKey = getBeanKey(sourceObj.getClass(), destObj.getClass(), converter);
        BeanCopier beanCopier = null;
        if (!BEAN_COPIER_MAP.containsKey(beanKey)) {
            boolean useConverter = false;
            if (converter != null) {
                useConverter = true;
            }
            beanCopier = BeanCopier.create(sourceObj.getClass(), destObj.getClass(), useConverter);
            BEAN_COPIER_MAP.put(beanKey, beanCopier);
        } else {
            beanCopier = BEAN_COPIER_MAP.get(beanKey);
        }
        beanCopier.copy(sourceObj, destObj, converter);
    }

    /**
     * array list copy
     *
     * @param sourceList source array list
     * @param targetList target array list
     * @param <T> class object
     * @param clazz class object
     * @param converter  converter
     */
    public static <T> void copy(List<?> sourceList, List<T> targetList, Class<T> clazz, Converter converter) {
        for (int i = 0; i < sourceList.size(); i++) {
            Object source = sourceList.get(i);
            T target = null;
            boolean exists = false;
            if (targetList.size() > i) {
                target = targetList.get(i);
                exists = true;
            }
            if (target == null) {
                try {
                    target = clazz.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if (target != null) {
                copy(source, target, converter);
            }
            if (exists) {
                targetList.set(i, target);
            } else {
                targetList.add(target);
            }

        }
    }

    /**
     * array list with converter
     * @param <T> class object
     * @param sourceList source array list
     * @param clazz      target class
     * @param converter  converter
     * @return List
     */
    public static <T> List<T> copy(List<?> sourceList, Class<T> clazz, Converter converter) {
        if (sourceList == null || sourceList.isEmpty()) {
            return new ArrayList<>(0);
        }
        List<T> targetList = new ArrayList<>(sourceList.size());
        copy(sourceList, targetList, clazz, converter);
        return targetList;
    }

    /**
     * array list copy
     * @param <T> class object
     * @param sourceList source array list
     * @param clazz      target class
     * @return List
     */
    public static <T> List<T> copy(List<?> sourceList, Class<T> clazz) {
        if (sourceList == null || sourceList.isEmpty()) {
            return new ArrayList<>(0);
        }
        List<T> targetList = new ArrayList<>(sourceList.size());
        copy(sourceList, targetList, clazz, null);
        return targetList;
    }

    /**
     * get bean key
     *
     * @param sourceClass source class
     * @param destClass   destination class
     * @param converter   converter
     * @return String
     */
    private static String getBeanKey(Class<?> sourceClass, Class<?> destClass, Converter converter) {
        String key = sourceClass.getName() + destClass.getName();
        if (converter != null) {
            key += converter.getClass().getName();
        }
        return key;
    }
}
