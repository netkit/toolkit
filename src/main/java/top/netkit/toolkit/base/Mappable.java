package top.netkit.toolkit.base;

import top.netkit.toolkit.util.JsonUtil;

import java.util.Map;

/**
 * mappable
 * @author shixinke
 */
public interface Mappable<K, V> {

    /**
     * object to map
     * @return Map
     */
    default Map<K, V> toMap() {
        return JsonUtil.toMap(this);
    }
}
