package top.netkit.toolkit.base;

/**
 * cache key interface
 * @author shixinke
 */
public interface CacheKeyAble {
    /**
     * get cache key
     * @return String
     */
    String getKey();

    /**
     * get cache expire
     * @return long
     */
    long getExpire();

    /**
     * get cache remark
     * @return String
     */
    String getRemark();

    /**
     * get cache key with placeholder
     * @param args args
     * @return String
     */
    default String getKey(Object...args) {
        return String.format(getKey(), args);
    }
}
