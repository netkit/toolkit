package top.netkit.toolkit.base;

import net.sf.cglib.core.ReflectUtils;
import top.netkit.toolkit.util.BeanCopierUtil;

/**
 * convertible
 * @author shixinke
 */
public interface Convertible {

    /**
     * object convert
     * @param clazz target class
     * @param <T> class object
     * @return T
     */
    default <T> T convert(Class<T> clazz) {
        T targetObject = null;
        try {
            targetObject = (T) ReflectUtils.newInstance(clazz);
            BeanCopierUtil.copy(this, targetObject, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return targetObject;
    }
}
