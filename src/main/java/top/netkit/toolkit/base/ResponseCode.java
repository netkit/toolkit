package top.netkit.toolkit.base;

/**
 * response error
 * @author shixinke
 */
public interface ResponseCode {
    /**
     * response code
     * @return int
     */
    int getCode();

    /**
     * response message
     * @return String
     */
    String getMessage();

    /**
     * is success
     * @return boolean
     */
    boolean isSuccess();
}
