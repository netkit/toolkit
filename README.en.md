
# Toolkit

#### introduction

- util： 
    - bean copier util
    - json serializer util
- common interface
    - cache key interface: CacheKeyAble
    - bean copier interface: Convertible
    - map interface: Mappable
    - response code interface: ResponseCode
- common exception
    - response exception： ResponseException
    - business exception: ServiceException
    - validate exception: ValidateException
- common response bean
    - api response: ApiResponse
    - pagination bean： PageList


#### usage

##### 1.add maven dependency

```xml
<dependency>
    <groupId>top.netkit</groupId>
    <artifactId>toolkit</artifactId>
    <version>1.0.2</version>
</dependency>

```

##### 2. use the package

(1)bean copier util

```java
import top.netkit.toolkit.base.Convertible;
public class Item implements Convertible {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
}

public class ItemVO {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
}

public class Demo {

    public static void main(String[] args){
        Item item = new Item();
        //其他操作查询出item信息      
        ItemVO itemVO = item.convert(ItemVO.class);
    }

}
```
(2)json util

```java
public class Demo {
    public static void main(String[] args){
      String data = "{\"id\":1,\"name\":\"netkit\"}";
      Map<String, Object> dataMap = JsonUtil.parseObject(data, Map.class);
      String jsonString = JsonUtil.toJsonString(dataMap);
    }
}
```

#### Contribution

1.  Fork the repository
2.  Create feature/featureName branch
3.  Commit your code
4.  Create Pull Request

